<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ItGirls
 */

get_template_part('header', 'interno'); ?>

	<div class="container container-interno">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-8 col-sm-12 col-md-offset-2">
					<?php
					while (have_posts()) {
						the_post();
						?>
						<article class="article-blog" id="post-<?php the_ID() ?>" <?php post_class() ?>>
							<header class="header-blog">
								<h1><?php the_title() ?></h1>
								<time datetime="<?php the_time('c') ?>">Data de Publicação: <?php the_time(get_option('date_format')) ?></time>
							</header>

							<div>
								<?php the_content() ?>
							</div>
						</article>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

<?php
/*get_sidebar();*/
get_footer();
