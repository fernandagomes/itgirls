<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ItGirls
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
		<nav id="navbartopo" class="navbar navbar-default navbar-fixed-top efeito-cores">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand smoothScroll" href="http://localhost/wordpress/#"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-brand.png" alt="ItGirls" class="img-logo" /></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					 <?php
							wp_nav_menu( array(
								'menu'              => 'primary',
								'theme_location'    => 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
								'walker'            => new wp_bootstrap_navwalker())
							);
						?>
										
						<ul class="nav navbar-nav navbar-right">
							<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ul.png" alt="Ulbra" class="img-nav" /></a></li>
							<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/Cursos/Ciencia-da-Computacao/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc.png" alt="Ciência da Computação" class="img-nav" /></a></li>
							<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/Cursos/Sistemas-de-Informacao/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/si.png" alt="Sistemas de Informação" class="img-nav" /></a></li>
							<li class="hidden-xs hidden-sm"><a href="http://fabrica.ulbra-to.br/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/fws.png" alt="Fábrica de Software"  class="img-nav" /></a></li>
						</ul>
						<?php
							wp_nav_menu( array(
								'menu'              => 'second',
								'theme_location'    => 'second',
								'depth'             => 2,
								'container'         => 'div',
								'menu_class'        => 'nav navbar-nav navbar-right',
								'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
								'walker'            => new wp_bootstrap_navwalker())
							);
						?>
				</div>
			</div>
	</nav>
	<div id="content" class="site-content">
