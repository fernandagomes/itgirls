<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ItGirls
 */

get_template_part('header', 'interno'); ?>

	<div class="container container-interno">
		<div class="row">
					<?php
						while (have_posts()) {
							the_post();
							?>
				<article class="article-minibiografia" id="post-<?php the_ID() ?>" <?php post_class() ?>>
					<div class="col-sm-5">
					<header class="header-blog">
						<h1><?php the_title() ?></h1>
						<time datetime="<?php the_time('c') ?>">Data de Publicação: <?php the_time(get_option('date_format')) ?></time>
					</header>
							<?php echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><img src="' . get_image_path($post->ID) . '" class="img-responsive" /></a>'; ?>
				    </div>
					<div class="col-sm-7">
						<div>
							<?php the_content() ?>
						</div>
					</div>
				</article>
					<?php } ?>
				</div>
			</div>

<?php
/*get_sidebar();*/
get_footer();
