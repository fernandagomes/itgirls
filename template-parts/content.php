<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ItGirls
 */

?>

<?php the_title( '<strong><p>', '</p></strong>' ); 
the_content();
?>
