<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ItGirls
 */

?>

	</div><!-- #content -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 text-left">
					<i class="fa fa-map-marker fa-fw"></i> Av. Teotonio Segurado, 1501 Sul – CEULP/ULBRA <br /> <i class="fa fa-phone fa-fw" style="margin-left: 5px;"></i> (63) 3219-8125 <br />
			        <i class="fa fa-map-o fa-fw" style="margin-left: 5px;"></i> <a href="https://www.google.com/maps/@-10.278002,-48.333183,17z?hl=pt-BR" style="color: #fff;" target="_blank">Ver no Google Maps</a><br /><br />
					<i class="fa fa-copyright fa-fw"></i> 2016 — ItGirls
				</div>
				<nav id="navbarbottom" class="col-lg-5 col-md-5 col-sm-4 hidden-xs">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/ul.png" alt="Ulbra" class="img-nav" /></a></li>
						<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/Cursos/Ciencia-da-Computacao/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc.png" alt="Ciência da Computação" class="img-nav" /></a></li>
						<li class="hidden-xs hidden-sm"><a href="http://ulbra-to.br/Cursos/Sistemas-de-Informacao/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/si.png" alt="Sistemas de Informação" class="img-nav" /></a></li>
						<li class="hidden-xs hidden-sm"><a href="http://fabrica.ulbra-to.br/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/fws.png" alt="Fábrica de Software"  class="img-nav" /></a></li>
					</ul>
					<?php
					wp_nav_menu( array(
							'menu'              => 'second',
							'theme_location'    => 'second',
							'depth'             => 2,
							'container'         => 'div',
							'menu_class'        => 'nav navbar-nav navbar-right',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
					);
					?>
				</div>
				</nav>
			<div>
		</div>
	</footer>
    
</div><!-- #page -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<?php wp_footer(); ?>

</body>
</html>
