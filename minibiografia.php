<?php
/*
 Template Name: MiniBiografias
 */
?>

<?php get_template_part('header', 'interno'); ?>
<div class="efeito-cores header-titulo-blog">
    <h1>Mulheres na TI</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <article class="article-blog">
                <?php
                $temp = $wp_query; $wp_query= null;
                $wp_query = new WP_Query(); $wp_query->query('showposts=5&post_type=minibiografias' . '&paged='.$paged);
                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <div class="caixa-post">
                        <div class="row">
                            <div class="col-md-3">
                                <?php
                                echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><img src="' . get_image_path($post->ID) . '" class="img-responsive" /></a>';
                                ?>
                            </div>
                            <div class="col-md-9">
                                <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                                <?php the_excerpt(); //resumo ?>
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <time datetime="<?php the_time('c') ?>"><?php the_time(get_option('date_format')) ?></time>
                            </div>
                        </div>
                    </div>
                <?php endwhile;  post_pagination(); wp_reset_postdata();?>
            </article>
        </div>
    </div>
</div>


<?php get_footer(); ?>
