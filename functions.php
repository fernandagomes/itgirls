<?php
/**
 * ItGirls functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ItGirls
 */

if ( ! function_exists( 'itgirls_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function itgirls_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ItGirls, use a find and replace
	 * to change 'itgirls' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'itgirls', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	
	require_once('wp_bootstrap_navwalker.php');
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'MenuHeader', 'itgirls' ),
		'second' => esc_html__( 'MenuRight', 'itgirls' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'itgirls_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'itgirls_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function itgirls_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'itgirls_content_width', 640 );
}
add_action( 'after_setup_theme', 'itgirls_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function itgirls_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'itgirls' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'itgirls' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'itgirls_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function itgirls_scripts() {
	wp_enqueue_style( 'itgirls-style', get_stylesheet_uri() );

	wp_enqueue_script( 'itgirls-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'itgirls-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'itgirls_scripts' );
add_post_type_support('page','excerpt');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* thumbnail da pagina de blog*/
function get_image_path ($post_id = null) {
    if ($post_id == null) {
        global $post;
        $post_id = $post->ID;
    }
    $theImageSrc = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
    global $blog_id;
    if (isset($blog_id) && $blog_id > 0) {
        $imageParts = explode('/files/', $theImageSrc);
        if (isset($imageParts[1])) {
            $theImageSrc = '/blogs.dir/' . $blog_id . '/files/' . $imageParts[1];
        }
    }
    return $theImageSrc;
}

/* paginacao*/
function post_pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class='paginacao'><span>P&aacute;ginas</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."' class='current'>&laquo;</a>";
        if($paged > 6 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>1</a> <span class='current'>...</span>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
            }
        }

        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<span class='current'>...</span> <a href='".get_pagenum_link($pages)."'>$pages</a>";
        if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."' class='current'>&raquo;</a>";
        echo "</div>";
    }
}

// Register Post Type Minibiografias
function minibiografias() {

    $labels = array(
        'name'                  => _x( 'Mini Biografias', 'Post Type General Name', 'minibiografias_domain' ),
        'singular_name'         => _x( 'Mini Biografia', 'Post Type Singular Name', 'minibiografias_domain' ),
        'menu_name'             => __( 'Minibiografias', 'minibiografias_domain' ),
        'name_admin_bar'        => __( 'Minibiografias', 'minibiografias_domain' ),
        'archives'              => __( 'Arquivos de Minibiografias', 'minibiografias_domain' ),
        'parent_item_colon'     => __( 'Minibiografia Mãe:', 'minibiografias_domain' ),
        'all_items'             => __( 'Todas as Minibiografias', 'minibiografias_domain' ),
        'add_new_item'          => __( 'Adicionar Nova', 'minibiografias_domain' ),
        'add_new'               => __( 'Adicionar Nova', 'minibiografias_domain' ),
        'new_item'              => __( 'Nova Minibiografia', 'minibiografias_domain' ),
        'edit_item'             => __( 'Editar Minibiografia', 'minibiografias_domain' ),
        'update_item'           => __( 'Atualizar Minibiografia', 'minibiografias_domain' ),
        'view_item'             => __( 'Visualizar Minibiografia', 'minibiografias_domain' ),
        'search_items'          => __( 'Procurar Minibiografia', 'minibiografias_domain' ),
        'not_found'             => __( 'Minibiografias não encontradas', 'minibiografias_domain' ),
        'not_found_in_trash'    => __( 'Minibiografia não encontrada na Lixeira', 'minibiografias_domain' ),
        'featured_image'        => __( 'Imagem Destacada', 'minibiografias_domain' ),
        'set_featured_image'    => __( 'Definir imagem destacada', 'minibiografias_domain' ),
        'remove_featured_image' => __( 'Remover imagem destacada', 'minibiografias_domain' ),
        'use_featured_image'    => __( 'Usar como imagem destacada', 'minibiografias_domain' ),
        'insert_into_item'      => __( 'Inserir no editor', 'minibiografias_domain' ),
        'uploaded_to_this_item' => __( 'Enviado para este', 'minibiografias_domain' ),
        'items_list'            => __( 'Lista de Minibiografias', 'minibiografias_domain' ),
        'items_list_navigation' => __( 'Navegação de item de Minibiografias', 'minibiografias_domain' ),
        'filter_items_list'     => __( 'Filtrar lista de Minibiografias', 'minibiografias_domain' ),
    );
    $rewrite = array(
        'slug'                  => 'minibiografia',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __( 'Mini Biografia', 'minibiografias_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-quote',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type( 'minibiografias', $args );

}
add_action( 'init', 'minibiografias', 0 );