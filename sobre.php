<?php
/*
 Template Name: Sobre
 */
?>

<?php get_template_part('header', 'interno'); ?>
    <div class="efeito-cores header-titulo-blog">
        <h1>Sobre</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
<?php
while ( have_posts() ) : the_post();?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php
                        the_content();

                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'itgirls' ),
                            'after'  => '</div>',
                        ) );
                        ?>


                </article><!-- #post-## -->
    <?php endwhile; // End of the loop.
?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>