<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ItGirls
 */

get_header(); ?>

<section id="blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
						<div id="carousel-posts-recentes" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">
								<?php
								$cont = 0;
								query_posts("showposts=3&post_type=post");
								if( have_posts() ) : while ( have_posts()) : the_post(); $cont++ ?>

								<?php if($cont == 1){?>
								<div class="item active" style="background: url(<?php echo get_image_path($post->ID) ?>) center center no-repeat; background-size: cover">
								<?php } else{?>
								<div class="item" style="background: url(<?php echo get_image_path($post->ID) ?>) center center no-repeat; background-size: cover">
								<?php } ?>
									<a href="<?php the_permalink();?>">
										<div class="carousel-img">

										</div>
									</a>
									<div class="carousel-top-caption">
											<h1>Blog</h1>
									</div>
									<div class="carousel-caption">
										<a href="<?php the_permalink();?>">
											<h2><?php the_title();?></h2>
										</a>
									</div>
								</div>
								<?php endwhile; endif; wp_reset_query();?>
									<a class="left carousel-control" href="#carousel-posts-recentes" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#carousel-posts-recentes" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
							</div>

						</div>


					</div>
				</div>
			</div>
</section>
	
	<section id="biografia">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2> Mulheres na TI </h2>
				</div>
				<div class="col-sm-6 col-xs-12">
					<?php
						query_posts("showposts=1&post_type=minibiografias&category_name=destaque");
						if( have_posts() ) : while ( have_posts()) : the_post(); ?>
							<a href="<?php the_permalink();?>" rel="bookmark">
								<div class="perfil" style="background: url(<?php echo get_image_path($post->ID) ?>) center center no-repeat; background-size: cover;"></div>
								<!-- <div class="minibiografia-caption">
									<h1>Destaque do Mês</h1>
								</div> -->
								<div class="nome">
									<?php the_title()?>
								</div>
							</a>
					<?php endwhile; endif; wp_reset_query();?>
				</div>
				<div class="col-sm-6 col-xs-12">
					<?php
						query_posts("showposts=1&post_type=minibiografias&category_name=geral");
						if( have_posts() ) : while ( have_posts()) : the_post(); ?>
							<a href="<?php the_permalink();?>" rel="bookmark">
								<div class="perfil" style="background: url(<?php echo get_image_path($post->ID) ?>) center center no-repeat; background-size: cover"></div>

								<div class="nome">
									<?php the_title()?>
								</div>
							</a>
					<?php endwhile; endif; wp_reset_query();?>
				</div>
			</div>
		</div>
	</section>

	<section id="encontros">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2> Encontros </h2>

					<?php // Retrieve the next 5 upcoming events
					$events = tribe_get_events( array(
						'posts_per_page' => 2,
						'eventDisplay' => 'list',
					) );
					// Loop through the events, displaying the title
					// and content for each
					$numEvents=count($events);
					if($numEvents==1){
							echo '<div class="col-md-6 col-md-offset-3">
									<div class="panel">';
							echo '		<div class="panel-body"> 
										<a href="'.tribe_get_event_link($events[0]).'">
										<h3>'.$events[0]->post_title. '</h3>';
							echo tribe_get_start_date( $events[0]).'</h5></a></div></div></div>';
					}
					else {
						foreach ($events as $event) { 
							echo '<div class="col-md-6">
										<div class="panel">';
							echo '		<div class="panel-body"> 
											<a href="' . tribe_get_event_link($event) . '">
											<h3>' . $event->post_title . '</h3>';
							echo tribe_get_start_date($event) . '</h5></a></div></div></div>';
						}
					}?>
                    <div class="text-center col-md-12 .col-md-offset-3"><a href="http://localhost:8080/wordpress/encontros" class="btn btn-default btn-lg btn-calendar"><i class="fa fa-calendar" aria-hidden="true"></i> Ver Calendário</a></div>

                </div>
			</div>
		</div>
	</section>

	<section id="mapa">
        <h2 class="text-center">Local</h2>
		<div class="wrappermap">
			<div id="map"></div>
		</div>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	</section>
	

<?php
get_footer();